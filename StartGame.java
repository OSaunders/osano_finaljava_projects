import greenfoot.*;

/**
 * Write a description of class StartGame here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class StartGame extends World
{

    GreenfootSound backgroundMusic = new GreenfootSound("8bit Dungeon Level.mp3");
    /**
     * Constructor for objects of class StartGame.
     * 
     */
    public StartGame()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(800, 480, 1); 
        backgroundMusic.playLoop();
        prepare();
    }


    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {
        Start start = new Start();
        addObject(start, 385, 373);
        start.setLocation(390, 372);
        Logo logo = new Logo();
        addObject(logo, 400, 108);
    }
}

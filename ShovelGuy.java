import greenfoot.*;

/**
 * Write a description of class ShovelGuy here.
 * 
 * @author osano@email.uscb.edu 
 * @version (a version number or a date)
 */
public class ShovelGuy extends Actor
{
    //instance variab;es
    private GreenfootImage[] images = new GreenfootImage[9];
    private int frameCount;
    private int imageIndex;
    private boolean attacking;

    public ShovelGuy()
    {
        frameCount = 0;

        /*images[0] = new GreenfootImage("frame0.png");
        images[1] = new GreenfootImage("frame1.png");
        images[2] = new GreenfootImage("frame2.png");
        images[3] = new GreenfootImage("frame3.png");
        images[4] = new GreenfootImage("frame4.png");
        images[5] = new GreenfootImage("frame5.png");
        images[6] = new GreenfootImage("frame6.png");
        images[7] = new GreenfootImage("frame8.png");*/

    }//end ShovelGuy constructor
    /**
     * Act - do whatever the ShovelGuy wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */

    public void act() 
    {
        //frameCount++;
        checkKeyPress();
        checkCollision();

        if(frameCount == 7)
        {
            updateImage();
            frameCount = 0;
        }
    }//end method act  

    public void updateImage()
    {
        setImage(images[imageIndex]);

        if( ++imageIndex > (images.length -1))
        {
            imageIndex = 0;
        }
    }

    /**
     * Check whether a keyboard key has been pressed and react if it has.
     */
    private void checkKeyPress()
    {
        if (Greenfoot.isKeyDown("up")) 
        {
            setLocation(getX(), getY()-3);
            if( isTouching(Stone.class))
            {
                if(attacking == false)
                {
                    setLocation(getX(), getY()+3);
                }
                else
                {
                    removeTouching(Stone.class);
                }
            }

            if( getY()<176)
            {
                setLocation(getX(), getY()+3);
            }
        }

        if (Greenfoot.isKeyDown("down")) 
        {
            setLocation(getX(), getY()+3);
            if( isTouching(Stone.class))
            {
                if(attacking == false)
                {
                    setLocation(getX(), getY()-3);
                }
                else
                {
                    removeTouching(Stone.class);
                }
            }
        }

        if (Greenfoot.isKeyDown("right")) 
        {
            setLocation(getX()+3, getY());
            if( isTouching(Stone.class))
            {
                if(attacking == false)
                {
                    setLocation(getX()-3, getY());
                }
                else
                {
                    removeTouching(Stone.class);
                }
            }
        }

        if (Greenfoot.isKeyDown("left")) 
        {
            setLocation(getX()-3, getY());
            if( isTouching(Stone.class))
            {
                if(attacking == false)
                {
                    setLocation(getX()+3, getY());
                }
                else
                {
                    removeTouching(Stone.class);
                }
            }
        }

        if (Greenfoot.isKeyDown("space"))
        {
            //while space is pressed your in attack mode
            attacking = true;

        }
        else
        {
            attacking = false;
        }
    }

    private void checkCollision()
    {
        if (isTouching(Grass.class)) 
        {
            removeTouching(Grass.class);
        }//end if

        if (isTouching (Sand.class))
        {
            removeTouching(Sand.class);
        }//end if

        if(isTouching(Coin.class))
        {
            removeTouching(Coin.class); 
            Ruins ruins = (Ruins)getWorld();
            ruins.addScore(40);
        }//end if

        if(isTouching(Bar.class))
        {
            removeTouching(Bar.class);
            Ruins ruins = (Ruins)getWorld();
            ruins.addScore(500);
        }//end if

        if(isTouching (Zombie.class))
        {
            if(attacking == true)
            {
                removeTouching(Zombie.class);

            }
            else if( attacking == false)
            {
                // get a reference to the world
                Ruins ruins = (Ruins)getWorld();

                // call the world's public method
                ruins.runTime();
                // to substract time by some value

            }//end inner if
        }//end outter if

    }// end method checkCollision
}//end class ShovelGuy

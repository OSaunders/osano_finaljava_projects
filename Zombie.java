import greenfoot.*;

/**
 * Write a description of class Zombie here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Zombie extends Actor
{
    /**
     * Act - do whatever the Zombie wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        Walk();
        turnAtEdge();
        
        if ( isTouching(ShovelGuy.class) ) 
        {
            //removeTouching(ShovelGuy.class);
        }//
    }
    
    public void Walk()
    {
        move(2);
        if (isTouching (Stone.class))
        {
            move(-2);
            turn(180);
        }
        else if (isTouching (Sand.class))
        {
            move(-2);
            turn(180);
        }
    }
    
     /**
     * Check whether we are at the edge of the world. If we are, turn a 180 degrees.
     * If not, do nothing.
     */
    public void turnAtEdge()
    {
        if ( isAtEdge() ) 
        {
            turn(180);
        }//end if statement
    }//end turnAtEdge method
    
        
}

import greenfoot.*;

/**
 * Write a description of class Stone here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Stone extends Actor
{
    private GreenfootImage image1;
    private GreenfootImage image2;
    
    public void Stone()
    {
        image1 = new GreenfootImage("Stone.png");
        image2 = new GreenfootImage("Stone2.png");
        setImage(image1);
    }
    
    /**
     * Act - do whatever the Stone wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }
    
    private void checkCollision()
    { 
        
    }
}

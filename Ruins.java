import greenfoot.*;
import java.util.List;

/**
 * Write a description of class City1 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Ruins extends World
{
    Sand[][] sandblocks = new Sand[9][25];
    private int score;
    private int time;
   

    /**
     * Constructor for objects of class Ruins.
     * 
     */
    public Ruins()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(800, 480, 1);

        for( int y = 0; y < 9; y++ )
        {
            for(int x = 0; x < 25 ; x++ )
            {   
                sandblocks[y][x] = new Sand();
                if ( !( x == 4 && y == 4 ) &&
                !( x == 10 && y == 6 ) && 
                !( x == 10 && y == 7 ) &&
                !( x == 10 && y == 8 ) &&
                !( x == 11 && y == 8 ) &&
                !( x == 12 && y == 8 ) &&
                ! (x == 13 && y == 8 ) &&
                ! (x == 14 && y == 8 ) &&
                ! (x == 15 && y == 8 ) &&
                ! (x == 16 && y == 8 ) &&
                ! (x == 17 && y == 8 ) &&
                ! (x == 17 && y == 7 ) &&
                ! (x == 17 && y == 6 ) &&
                !( x == 10 && y == 6 ) &&
                !( x == 11 && y == 6 ) &&
                !( x == 12 && y == 6 ) &&
                ! (x == 13 && y == 6 ) &&
                ! (x == 14 && y == 6 ) &&
                ! (x == 15 && y == 6 ) &&
                ! (x == 16 && y == 6 ) &&
                ! (x == 17 && y == 6 ) &&
                ! (x == 11 && y == 7 ) &&
                ! (x == 12 && y == 7 ) &&
                ! (x == 13 && y == 7 ) &&
                ! (x == 14 && y == 7 ) &&
                ! (x == 15 && y == 7 ) &&
                ! (x == 16 && y == 7 ) &&
                ! (x == 20 && y == 3 ) &&
                ! (x == 3 && y ==3 ) &&
                ! (x == 7 && y == 7) &&
                ! (x == 15 && y == 2) &&
                ! (x == 16 && y == 2) &&
                ! (x == 14 && y == 2) &&
                ! (x == 13 && y == 2) &&
                ! (x == 23 && y ==8) &&
                ! (x == 24 && y ==8) &&
                ! (x == 24 && y == 7) &&
                ! (x == 23 && y == 7) &&
                ! (x == 0 && y == 8))
                {
                    addObject(new Sand(), (x*32 + 16),(y*32 + 16 + 192));
                }
            }
        }

        prepare();
        score = 0;
        time = 2500;
        showScore();
        showTime();
        
       
        
    }//end method ruins

    private void prepare()
    {

        Stone stone = new Stone();
        addObject(stone, 343, 407);
        stone.setLocation(336, 400);
        Stone stone2 = new Stone();
        addObject(stone2, 375, 406);
        stone2.setLocation(368, 400);
        Stone stone3 = new Stone();
        addObject(stone3, 407, 403);
        stone3.setLocation(439, 403);
        stone2.setLocation(368, 400);
        stone3.setLocation(400, 400);
        Stone stone4 = new Stone();
        addObject(stone4, 443, 403);
        stone4.setLocation(429, 403);
        stone4.setLocation(429, 400);
        stone4.setLocation(433, 399);
        stone4.setLocation(431, 399);
        stone4.setLocation(431, 400);
        stone2.setLocation(367, 400);
        stone3.setLocation(398, 400);
        stone4.setLocation(429, 400);
        Stone stone5 = new Stone();
        addObject(stone5, 493, 406);
        stone5.setLocation(460, 400);
        Stone stone6 = new Stone();
        addObject(stone6, 505, 400);
        stone6.setLocation(491, 400);
        Stone stone7 = new Stone();
        addObject(stone7, 531, 406);
        stone7.setLocation(522, 400);
        Stone stone8 = new Stone();
        addObject(stone8, 562, 407);
        stone8.setLocation(560, 400);
        stone7.setLocation(528, 400);
        stone6.setLocation(497, 400);
        stone5.setLocation(465, 400);
        stone4.setLocation(433, 400);
        stone3.setLocation(401, 400);
        stone2.setLocation(368, 399);
        stone2.setLocation(369, 400);
        stone.setLocation(337, 400);
        stone.setLocation(337, 400);
        stone.setLocation(336, 400);
        stone8.setLocation(560, 400);
        stone2.setLocation(368, 400);
        stone3.setLocation(400, 400);
        stone4.setLocation(432, 400);
        stone5.setLocation(464, 400);
        stone6.setLocation(496, 400);
        Stone stone9 = new Stone();
        addObject(stone9, 343, 441);
        stone9.setLocation(336, 432);
        Stone stone10 = new Stone();
        addObject(stone10, 342, 472);
        stone10.setLocation(336, 464);
        Stone stone11 = new Stone();
        addObject(stone11, 376, 470);
        stone11.setLocation(368, 464);
        Stone stone12 = new Stone();
        addObject(stone12, 414, 467);
        stone12.setLocation(400, 464);
        Stone stone13 = new Stone();
        addObject(stone13, 445, 469);
        stone13.setLocation(432, 464);
        Stone stone14 = new Stone();
        addObject(stone14, 478, 468);
        stone14.setLocation(464, 464);
        Stone stone15 = new Stone();
        addObject(stone15, 504, 471);
        stone15.setLocation(495, 465);
        stone15.setLocation(496, 464);
        Stone stone16 = new Stone();
        addObject(stone16, 535, 470);
        stone16.setLocation(528, 464);
        Stone stone17 = new Stone();
        addObject(stone17, 565, 466);
        stone17.setLocation(560, 465);
        Stone stone18 = new Stone();
        addObject(stone18, 563, 438);
        stone18.setLocation(560, 432);
        stone17.setLocation(560, 464);
        Coin coin = new Coin();
        addObject(coin, 151, 342);
        coin.setLocation(147, 336);
        Zombie zombie = new Zombie();
        addObject(zombie, 372, 436);
        zombie.setLocation(370, 432);
        Bar bar = new Bar();
        addObject(bar, 533, 440);
        bar.setLocation(527, 432);
        Zombie zombie2 = new Zombie();
        addObject(zombie2, 666, 310);
        zombie2.setLocation(659, 304);
        zombie2.setLocation(656, 304);
        Zombie zombie3 = new Zombie();
        addObject(zombie3, 115, 310);
        zombie3.setLocation(113, 304);
        Coin coin2 = new Coin();
        addObject(coin2, 244, 435);
        coin2.setLocation(239, 432);
        Stone stone19 = new Stone();
        addObject(stone19, 535, 274);
        stone19.setLocation(528, 272);
        Stone stone20 = new Stone();
        addObject(stone20, 501, 276);
        stone20.setLocation(496, 272);
        Stone stone21 = new Stone();
        addObject(stone21, 758, 437);
        stone21.setLocation(752, 432);
        Stone stone22 = new Stone();
        addObject(stone22, 793, 438);
        stone22.setLocation(784, 432);
        Stone stone23 = new Stone();
        addObject(stone23, 758, 471);
        stone23.setLocation(752, 464);
        Bar bar2 = new Bar();
        addObject(bar2, 793, 473);
        bar2.setLocation(786, 466);
        Stone stone24 = new Stone();
        addObject(stone24, 439, 279);
        stone24.setLocation(432, 272);
        Stone stone25 = new Stone();
        addObject(stone25, 468, 277);
        stone25.setLocation(464, 272);
        Zombie zombie4 = new Zombie();
        addObject(zombie4, 21, 471);
        zombie4.setLocation(15, 464);
        ShovelGuy shovelguy = new ShovelGuy();
        addObject(shovelguy, 343, 180);
        shovelguy.setLocation(341, 174);
        shovelguy.setLocation(341, 176);
    }//end method prepare
    
    public void act()
    {
        countTime();
    }
    
     private void showScore()
    {
         showText("Score: " + score, 80, 25);
    }
    
    public void addScore(int points)
    {
       
        score = score + points;
        showScore();
        if(score < 0)
        {
            Greenfoot.stop();
        }
    }
    
    private void showTime()
    {
        showText("Time: "+ time, 700, 25);
    }
    
    private void countTime()
    {
        showTime();
        time--;
        if(time < 1) 
        {
            time = 0;
            showTime();
            showEndMessage();
            Greenfoot.stop();
        }
    }
    
    public void runTime()
    {
        time-=5;
        /*
        showTime();
        if(time == 0)
        {
            showEndMessage();
            Greenfoot.stop();
        }
        */
    }
    
    private void showEndMessage()
    {
        showText("Time's Up!", 390, 150);
        showText("Your final score: " + score + " points", 390, 170);
    }
    
    
}//end class Ruins
